import { Controller, Get, CACHE_MANAGER, Inject, Post, Body } from '@nestjs/common';
import {Cache} from 'cache-manager';
import { exist } from 'joi';
import { Sequelize } from 'sequelize-typescript';
import { SessionMiddleware } from './app.middleware';
import { AppService } from './app.service';
import { DatabaseProviders } from './database/providers/database.providers';


export interface Gamma {
  gamma: string
}


@Controller()
export class AppController {
  private readonly connect: Sequelize;
  fakeData:Gamma = {
    gamma: '81A9EA'
  }
  constructor(private readonly appService: AppService,
    @Inject(CACHE_MANAGER) private cacheManager: Cache,
    private readonly sessionMiddleware: SessionMiddleware,
    private readonly sequelize: DatabaseProviders,
    ) {
      this.connect = this.sequelize.connect;
    }

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }

  @Post('/cache')
  async getCache(@Body() gamma: Gamma){
    const device = gamma.gamma
    delete gamma.gamma
    const exists = await this.sessionMiddleware.get(device)
    if(exists){
      return exists
    }
    else{

      //consulta Base de datos
      const data = await this.connect.query(` 
          SELECT
                  d.identificador gamma, convert(bit,1) comunicando, convert(bit,0) errorHub
          FROM
                  tbl_dispositivo d
          INNER JOIN
                      tbl_gamma tg
                  ON d.id = tg.dispositivoId
          WHERE
                  d.identificador = '${device}';
      `);

      let setValue = data[0][0]?data[0][0]:{gamma: device, data: 'device no registrado'}
      
      //redis set
      // delete setValue.gamma
      await this.sessionMiddleware.set(device, setValue)
      //redis get
      return await this.sessionMiddleware.get(device)
    }
    // var data = await this.cacheManager.get<Gamma>('gamma-comunicando');
    // if(data){
    //   return {
    //     data:data,
    //     loadsFrom: 'cache'
    //   };
    // }
 
    // await this.cacheManager.set<Gamma>('gamma-comunicando', gamma, {ttl: 300});
    // return{
    //   data: gamma,
    //   loadFrom:'fake data base'
    // }
  }

  @Get('/healthcheck')
  healthcheck(): string {
    return this.appService.healthcheck();
  }
}
