import { Field, Int, ObjectType } from "@nestjs/graphql";

@ObjectType()
export class GeneralOutputGGC{
    @Field(() => Int, { nullable: true })
    affects_rows
    
    @Field(() => String, { nullable: true })
    error: string

    @Field(() => Int, { nullable: true })
    id: number

}