import { Module, CacheModule } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ConfigService } from '@nestjs/config';
import * as Joi from 'joi';
import * as redisStore from 'cache-manager-redis-store';

import { AppController } from './app.controller';
import { AppService } from './app.service';

// Enviroments config
import { enviroment } from './enviropments';
import configuration from './config/configuration';
import { SessionMiddleware } from './app.middleware';
import { DatabaseModule } from './database/database.module';

@Module({
  imports: [
    DatabaseModule,
    ConfigModule.forRoot({
      envFilePath: enviroment[process.env.NODE_ENV] || enviroment['dev'],
      isGlobal: true,
      load: [configuration],
      validationSchema: Joi.object({
        REDIS_HOST: Joi.string().required(),
        REDIS_PORT: Joi.number().required(),
        REDIS_PASSWORD: Joi.string().required(),
        DB_HOST: Joi.string().required(),
        DB_PORT: Joi.number().required(),
        DB_NAME: Joi.string().required(),
        DB_USER: Joi.string().required(),
        DB_PASSWORD: Joi.string().required(),
      })
    }),
    CacheModule.register({
      store: redisStore,
      host: new ConfigService().get<string>('REDIS_HOST'),
      port: new ConfigService().get<number>('REDIS_PORT'),
      auth_pass: new ConfigService().get<string>('REDIS_PASSWORD'),
      ttl: 60 * 60 * 24 * 48, // se guarda en redis por dos dias
      max: 2500,
    }),
  ],
  controllers: [AppController],
  providers: [AppService, SessionMiddleware],
})
export class AppModule {}
