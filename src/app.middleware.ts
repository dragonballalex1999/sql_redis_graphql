import {
  CACHE_MANAGER,
  Inject,
  Injectable,
} from '@nestjs/common';
import { Cache } from 'cache-manager';

@Injectable()
export class SessionMiddleware{

  constructor(@Inject(CACHE_MANAGER) private cacheManager: Cache) {}

  async set(gammaID: string, data: any): Promise<void> {
    await this.cacheManager.set(gammaID, data);
  }

  async del(gammaID: string): Promise<void> {
    await this.cacheManager.del(gammaID);
  }

  async get(gammaID: string): Promise<any> {
    return await this.cacheManager.get(gammaID);
  }
}
