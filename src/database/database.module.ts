import { Global, Module } from '@nestjs/common';
import { DatabaseProviders } from './providers/database.providers';


@Global()
@Module({
  imports: [],
  controllers: [],
  providers: [DatabaseProviders],
  exports: [DatabaseProviders],
})

export class DatabaseModule {}
