import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Sequelize, SequelizeOptions } from 'sequelize-typescript';

@Injectable()
export class DatabaseProviders {
  StringConnection: SequelizeOptions;
  readonly connect: Sequelize;

    constructor(private readonly configService: ConfigService){
      this.StringConnection = {
        dialect: configService.get('DIALECT'),
        host: configService.get<string>('DB_HOST'),
        port: configService.get<number>('DB_PORT'),
        username: configService.get<string>('DB_USER'),
        password: configService.get<string>('DB_PASSWORD'),
        database: configService.get<string>('DB_NAME'),
        pool: {
          min: 5,
          max: 100,
          idle: 100000,
          acquire: 10000,
          evict: 1000
      },
      retry: {
          max: 1
      },
      logging: false
    }
        this.connect = new Sequelize(this.StringConnection);
          try {
            // console.log('Connection has been established successfully.');
          } catch (error) {
            console.error('Unable to connect to the database:', error);
          }
    }
     
  }