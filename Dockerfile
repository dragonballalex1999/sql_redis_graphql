FROM node:14.17-alpine as develop

ARG DEBIAN_FRONTEND=noninteractive
RUN apk add tzdata
ENV TZ=America/Mexico_City

WORKDIR /usr/src/app
COPY package*.json ./
COPY tsconfig*.json ./
COPY yarn.lock .
# COPY src/assets/ ./src/assets/
RUN yarn install --frozen-lockfile

COPY .env .
COPY src/ ./src/
RUN yarn build
CMD ["yarn", "start:dev"]
